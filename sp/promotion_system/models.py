# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Brand(models.Model):
    brandid = models.AutoField(db_column='brandID', primary_key=True)  # Field name made lowercase.
    brandname = models.CharField(db_column='brandName', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'brand'


class Class(models.Model):
    classid = models.AutoField(db_column='classID', primary_key=True)  # Field name made lowercase.
    classname = models.CharField(db_column='className', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'class'


class Customer(models.Model):
    customerid = models.BigAutoField(db_column='customerID', primary_key=True)  # Field name made lowercase.
    customer_lastname = models.CharField(max_length=45)
    customer_firstname = models.CharField(max_length=60)
    customer_middlename = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'customer'


class Customercontact(models.Model):
    customercontactid = models.BigIntegerField(db_column='customercontactID', primary_key=True)  # Field name made lowercase.
    email = models.CharField(max_length=45, blank=True, null=True)
    number = models.BigIntegerField(blank=True, null=True)
    customer_customerid = models.ForeignKey(Customer, models.DO_NOTHING, db_column='customer_customerID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customercontact'
        unique_together = (('customercontactid', 'customer_customerid'),)


class Customerjsfcc(models.Model):
    cusjsfccid = models.BigAutoField(db_column='cusjsfccID', primary_key=True)  # Field name made lowercase.
    customer_customerid = models.ForeignKey(Customer, models.DO_NOTHING, db_column='customer_customerID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customerjsfcc'
        unique_together = (('cusjsfccid', 'customer_customerid'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Sku(models.Model):
    skuid = models.AutoField(db_column='skuID', primary_key=True)  # Field name made lowercase.
    shortdesc = models.CharField(db_column='shortDesc', max_length=60)  # Field name made lowercase.
    brand_brandid = models.ForeignKey(Brand, models.DO_NOTHING, db_column='brand_brandID')  # Field name made lowercase.
    class_classid = models.ForeignKey(Class, models.DO_NOTHING, db_column='class_classID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sku'
        unique_together = (('skuid', 'brand_brandid', 'class_classid'),)


class Transaction(models.Model):
    transactionid = models.BigIntegerField(db_column='transactionID', primary_key=True)  # Field name made lowercase.
    transactiondate = models.DateTimeField(db_column='transactionDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transaction'


class Transactioncard(models.Model):
    transactioncardid = models.BigAutoField(db_column='transactioncardID', primary_key=True)  # Field name made lowercase.
    transaction_transactionid = models.ForeignKey(Transaction, models.DO_NOTHING, db_column='transaction_transactionID')  # Field name made lowercase.
    customerjsfcc_cusjsfccid = models.ForeignKey(Customerjsfcc, models.DO_NOTHING, db_column='customerjsfcc_cusjsfccID')  # Field name made lowercase.
    customerjsfcc_customer_customerid = models.ForeignKey(Customerjsfcc, models.DO_NOTHING, db_column='customerjsfcc_customer_customerID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transactioncard'
        unique_together = (('transactioncardid', 'transaction_transactionid', 'customerjsfcc_cusjsfccid', 'customerjsfcc_customer_customerid'),)


class Transactiondetails(models.Model):
    transactiondetailsid = models.BigAutoField(db_column='transactionDetailsID', primary_key=True)  # Field name made lowercase.
    quantity = models.FloatField()
    transaction_transactionid = models.ForeignKey(Transaction, models.DO_NOTHING, db_column='transaction_transactionID')  # Field name made lowercase.
    sku_skuid = models.ForeignKey(Sku, models.DO_NOTHING, db_column='sku_skuID')  # Field name made lowercase.
    customerjsfcc_cusjsfccid = models.ForeignKey(Customerjsfcc, models.DO_NOTHING, db_column='customerjsfcc_cusjsfccID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transactiondetails'
        unique_together = (('transactiondetailsid', 'transaction_transactionid', 'sku_skuid', 'customerjsfcc_cusjsfccid'),)
