from django.apps import AppConfig


class PromotionSystemConfig(AppConfig):
    name = 'promotion_system'
